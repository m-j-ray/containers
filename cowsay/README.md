# [m-j-ray/cowsay](https://gitlab.com/m-j-ray/containers)

        ______________________________________
        < cowsay is a configurable talking cow >
        --------------------------------------
                \   ^__^
                 \  (oo)\_______
                    (__)\       )\/\
                        ||----w |
                        ||     ||

## Supported Architectures

| Architecture | Tag |
| :----: | --- |
| amd64 | x86_64 |
| arm64 | aarch64 |

## Version Tags

NA

## Usage

- Run on amd64

        podman run --rm quay.io/m-j-ray/cowsay:x86_64 cowsay "your message"

- Run on arm64 (raspberry pi)

        podman run --rm quay.io/m-j-ray/cowsay:aarch64 cowsay "your message"

## Parameters

| Parameter | Function |
| :---- | --- |
| `cowsay ` | output cow message |
