# [m-j-ray/ipxe-dnsmasq](https://gitlab.com/m-j-ray/containers)

![ipxe](https://ipxe.org/_media/logos/ipxe-small.png)

[iPXE](https://ipxe.org/)  is the leading open source network boot firmware. It provides a full PXE implementation enhanced with additional features.

![dnsmasq](https://www.thekelleys.org.uk/dnsmasq/images/icon.png)

[Dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) provides network infrastructure for small networks: DNS, DHCP, router advertisement and network boot.

## Supported Architectures

| Architecture | Tag |
| :----: | --- |
| amd64 | x86_64 |
| arm64 | aarch64 |

## Version Tags

NA

## Firewall

- If running `firewalld` you need to allow dhcp and tftp services.

        firewall-cmd --add-service=dhcp && \
        firewall-cmd --add-service=tftp


- **Optionally**, allow permanently.

        firewall-cmd --add-service=dhcp && \
        firewall-cmd --add-service=tftp && \
        firewall-cmd --permanent --add-service=dhcp && \
        firewall-cmd --permanent --add-service=tftp

## Usage

- Run on amd64

        sudo podman run -d \
        --network host \
        --cap-add ALL \
        -e X86_SCRIPT=http://webserver:8080/boot.ipxe \
        -e X86_EFI_SCRIPT=http://webserver:8080/boot.ipxe \
        -e ARM64_EFI_SCRIPT=http://webserver:8080/boot.ipxe \
        quay.io/m-j-ray/ipxe-dnsmasq:x86_64

- Run on arm64

        sudo podman run -d \
        --network host \
        --cap-add ALL \
        -e X86_SCRIPT=http://webserver:8080/boot.ipxe \
        -e X86_EFI_SCRIPT=http://webserver:8080/boot.ipxe \
        -e ARM64_EFI_SCRIPT=http://webserver:8080/boot.ipxe \
        quay.io/m-j-ray/ipxe-dnsmasq:aarch64

## Parameters

| Parameter | Function |
| :---- | --- |
| `-e X86_BOOTFILE` | iPXE image for X86 systems |
| `-e X86_SCRIPT` | iPXE script for X86 systems  |
| `-e X86_EFI_BOOTFILE` | iPXE image for X86 EFI systems |
| `-e X86_EFI_SCRIPT` | iPXE script for X86 EFI systems |
| `-e ARM64_EFI_BOOTFILE` | iPXE image for ARM64 EFI systems |
| `-e ARM64_EFI_SCRIPT` | iPXE script for ARM64 EFI systems |
