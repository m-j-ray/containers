# [m-j-ray/ipxe-nginx](https://gitlab.com/m-j-ray/containers)

![ipxe](https://ipxe.org/_media/logos/ipxe-small.png)

[iPXE](https://ipxe.org/)  is the leading open source network boot firmware. It provides a full PXE implementation enhanced with additional features.

![dnsmasq](https://nginx.org/nginx.png)

[NGiNX](http://www.thekelleys.org.uk/dnsmasq/doc.html) is a HTTP and reverse proxy server.

## Supported Architectures

| Architecture | Tag |
| :----: | --- |
| amd64 | x86_64 |
| arm64 | aarch64 |

## Version Tags

NA

## Usage

- Run on amd64

        podman run --rm -d -p 8080:80 quay.io/m-j-ray/ipxe-nginx:x86_64

- Run on arm64 (raspberry pi)

        podman run --rm -d -p 8080:80 quay.io/m-j-ray/ipxe-nginx:aarch64

## Parameters

| Parameter | Function |
| :---- | --- |
| `-e IPXE_*` | Passes matching vars to clients via `ipxe/vars.ipxe` for use in ipxe scripts |
