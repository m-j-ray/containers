# Containers

Build containers using Podman and Ansible.

## Required Packages

| Package   | Minimum Version |
|-----------|:---------------:|
| `podman`  | `2.2.1`         |
| `ansible` | `2.9`           |
| `qemu-user-static` | `5.1.0` |

## Required Ansible Collections

- `containers.podman`

## Optional Packages

| Package        | Minimum Version |
|----------------|:---------------:|
| `lastpass-cli` | `1.3.3`         |

## Registry Auth

Login to Quay.io is attempted in the following order:

- `${REGISTRY_AUTH_FILE}`
- `${XDG_RUNTIME_DIR}/containers/auth.json`
- `${HOME}/.docker/config.json`

If no authfiles exist, set creds from LastPass:
- `"{{lookup('lastpass', 'ansible/quay.io', field='user')}}"`
- `"{{lookup('lastpass', 'ansible/quay.io', field='pass')}}"`

## Examples

### Build Container

    ansible-playbook cowsay/playbook.yml -t build

### Push Container

    ansible-playbook cowsay/playbook.yml -t push

### Build & Push Container

    ansible-playbook cowsay/playbook.yml

### Build & Push Multiarch Container

    ansible-playbook cowsay/playbook.yml -e multiarch=1

## License

Apache-2.0 or MIT or BSD-3-Clause

## Author Information

[Matt Ray](https://gitlab.com/m-j-ray)
